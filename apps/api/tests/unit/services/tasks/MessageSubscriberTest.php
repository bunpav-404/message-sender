<?php
declare(strict_types=1);

namespace tests\unit\services\tasks;

use PHPUnit\Framework\TestCase;
use common\services\tasks\MessageSubscriber;
use common\services\tasks\MessengerObserverBuilder;
use common\services\tasks\MessengersTypeEnum;
use common\services\tasks\messenger\MessengerStructure;
use common\services\tasks\tunnels\TelegramTunnel;

class MessageSubscriberTest extends TestCase
{
    public function testInitFailStructure()
    {
        try {
            $subscriber = new MessageSubscriber(\Yii::$container, ['ss']);
            $this->assertTrue(false, 'Смогли неправильно сконструировать класс. не правильная структура');
        } catch (\Throwable $error) {
            $this->assertTrue(true);
        }
    }

    public function testInitFailObserver()
    {
        try {
            $subscriber = new MessageSubscriber(\Yii::$container, [['class' => TestCase::class, 'messengerType' => ''
                                                                   ]
            ]);
            $this->assertTrue(false, 'Смогли неправильно сконструировать класс. Подсунули не правильный наблюдатель');
        } catch (\Throwable $error) {
            $this->assertTrue(true);
        }
    }

    public function testInitSuccess()
    {
        $subscriber = new MessageSubscriber(\Yii::$container,
            [
                MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM)
            ]);
        $this->assertTrue(true);
    }

    public function testAttach()
    {
        $closure = function () {
            return $this->getEventObservers(MessengersTypeEnum::TELEGRAM);
        };
        $subscriber = new MessageSubscriber(\Yii::$container,
            [
                MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM)
            ]);
        $this->assertCount(1, $closure->call($subscriber), 'инитили только 1 наблюдатель, откуда больше?');
        $subscriber->attach(new TelegramTunnel(), MessengersTypeEnum::TELEGRAM);
        $this->assertCount(2, $closure->call($subscriber), 'Должны были добавить 1 наблюдатель');
    }

    public function testDetach()
    {
        $closure = function () {
            return $this->getEventObservers(MessengersTypeEnum::TELEGRAM);
        };
        $subscriber = new MessageSubscriber(\Yii::$container,
            [
                MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM)
            ]);
        $oserver = new TelegramTunnel();
        $this->assertCount(1, $closure->call($subscriber), 'инитили только 1 наблюдатель, откуда больше?');
        $subscriber->attach($oserver, MessengersTypeEnum::TELEGRAM);
        $this->assertCount(2, $closure->call($subscriber), 'Должны были добавить 1 наблюдатель');
        $subscriber->detach($oserver, MessengersTypeEnum::TELEGRAM);
        $this->assertCount(1, $closure->call($subscriber), 'должны были убрать один наблюдатель');
    }

    public function testNotify()
    {
        $subscriber = new MessageSubscriber(\Yii::$container,
            [
                MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM)
            ]);
        $data = new MessengerStructure();
        $data->setNeedSend(new \DateTime('2018-12-26 15:00'))
            ->setSendTo('Vasya')
            ->setMessage('hello');
        $result = $subscriber->notify(MessengersTypeEnum::TELEGRAM, $data);
        $this->assertCount(1, $result, 'инитили только 1 наблюдатель, откуда больше?');
        $this->assertContains('Telegram', current($result), 'пушили в телеграм');
    }
}