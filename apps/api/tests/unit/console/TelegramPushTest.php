<?php
declare(strict_types=1);

namespace tests\unit\console;

use PHPUnit\Framework\TestCase;
use console\controllers\TelegramPush;

class TelegramPushTest extends TestCase
{
    public function testPushValidPastDate()
    {
        $date = new \DateTime('now', new \DateTimeZone('+0300'));
        $date->sub(new \DateInterval('PT10S'));
        $userName = 'user-1';
        $message = 'push message';
        $push = new TelegramPush($userName, $date, $message);
        $result = $push->execute(null);
        $this->assertContains($userName, $result);
        $this->assertContains($message, $result);
        $this->assertContains('Telegram', $result);
    }

    public function testPushValidFutureDate()
    {
        $date = new \DateTime('now', new \DateTimeZone('+0300'));
        $date->add(new \DateInterval('PT10S'));
        $userName = 'user-1';
        $message = 'push message';
        $push = new TelegramPush($userName, $date, $message);
        $result = $push->execute(null);
        $this->assertContains('resend', $result);
    }

    public function testPushValidNowDate()
    {
        $date = new \DateTime('now', new \DateTimeZone('+0300'));
        $userName = 'user-1';
        $message = 'push message';
        $push = new TelegramPush($userName, $date, $message);
        $result = $push->execute(null);
        $this->assertContains($userName, $result);
        $this->assertContains($message, $result);
        $this->assertContains('Telegram', $result);
    }
}