<?php
declare(strict_types=1);

namespace console\controllers;

class ViberPush extends AbstractPushJob
{
    protected $messengerName = 'Viber';
}