<?php
declare(strict_types=1);

namespace common\services\tasks\messenger;

use common\services\tasks\contract\MessengerStructureContract;

/**
 * Class MessengerStructure
 *
 * Структура сообщения для отправки в мессенджеры
 * @package common\services\tasks\messenger
 */
class MessengerStructure implements MessengerStructureContract
{
    use MessengerStructureTrait;

}