<?php
declare(strict_types=1);

namespace common\services\tasks\tunnels;

use common\services\tasks\contract\MessengerStructureContract;
use console\controllers\ViberPush;

class ViberTunnel extends AbstractTunnel
{
    public function loadStructure(MessengerStructureContract $structure): MessengerStructureContract
    {
        $structure->setMessengerName('Viber');
        return parent::loadStructure($structure);

    }

    /**
     * Если установлена очередь, ставит задачу отправить сообщение в Viber
     *
     * @param \yii\queue\cli\Queue|null $queue
     * @return string
     */
    public function execute($queue): string
    {
        if ($queue !== null) {
            $queue->push(new ViberPush(
                    $this->getSendTo(),
                    $this->getNeedSend(),
                    $this->getMessage()
                )
            );
        }
        return parent::execute($queue);
    }
}