<?php
declare(strict_types=1);

namespace common\services\tasks\tunnels;

use common\services\tasks\contract\MessengerStructureContract;
use console\controllers\WhatsappPush;

class WharsappTunnel extends AbstractTunnel
{
    public function loadStructure(MessengerStructureContract $structure): MessengerStructureContract
    {
        $structure->setMessengerName('Whatsapp');
        return parent::loadStructure($structure);

    }

    /**
     * Если установлена очередь, ставит задачу отправить сообщение в Whatsapp
     *
     * @param \yii\queue\cli\Queue|null $queue
     * @return string
     */
    public function execute($queue): string
    {
        if ($queue !== null) {
            $queue->push(new WhatsappPush(
                    $this->getSendTo(),
                    $this->getNeedSend(),
                    $this->getMessage()
                )
            );
        }
        return parent::execute($queue);
    }
}