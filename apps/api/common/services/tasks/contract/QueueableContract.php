<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use yii\queue\cli\Queue;

interface QueueableContract
{
    /**
     * Устанавливает обработчик очередей, для пушинга в демона
     *
     * @param Queue $queue
     * @return void
     */
    public function setQueue(Queue $queue): void;

    /**
     * Возвращает обработчик очередей
     *
     * @return Queue|null
     */
    public function getQueue(): ?Queue;
}