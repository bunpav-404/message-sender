<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use DateTime;

/**
 * Interface MessengerStructureContract
 *
 * Структура передаваемая в мессенджеры
 * @package common\services\tasks\contract
 */
interface MessengerStructureContract
{
    /**
     * Загружает переданную структуру в структуру класса
     *
     * @param MessengerStructureContract $structure
     * @return MessengerStructureContract
     */
    public function loadStructure(MessengerStructureContract $structure): MessengerStructureContract;

    /**
     * Возвражает наименование мессенджера
     * @return string
     */
    public function getMessengerName(): string;

    /**
     * Устанавливает наименование мессенджера
     * @param string $messengerName
     * @return MessengerStructureContract
     */
    public function setMessengerName(string $messengerName): MessengerStructureContract;

    /**
     * Возвращает ключ отправителя
     * @return string
     */
    public function getSendTo(): string;

    /**
     * Устанавливает ключ отправителя
     * @param string $sendTo
     * @return MessengerStructureContract
     */
    public function setSendTo(string $sendTo): MessengerStructureContract;

    /**
     * Возвращает тело сообщения
     * @return string
     */
    public function getMessage(): string;

    /**
     * Устанавливает тело сообщения
     * @param string $message
     * @return MessengerStructureContract
     */
    public function setMessage(string $message): MessengerStructureContract;

    /**
     * Возвращает дату, когда должно быть отправлено сообщение
     * @return DateTime
     */
    public function getNeedSend(): DateTime;

    /**
     * Устанавливает дату, когда должно быть отправлено сообщение
     * @param DateTime $needSend
     * @return MessengerStructureContract
     */
    public function setNeedSend(DateTime $needSend): MessengerStructureContract;


}