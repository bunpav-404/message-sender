<?php
declare(strict_types=1);

namespace common\services\tasks;

use common\services\tasks\contract\MessageSubscriberContract;
use common\services\tasks\contract\MessengerSenderContract;
use common\services\tasks\contract\MessengerStructureContract;
use common\services\tasks\contract\QueueableContract;
use yii\queue\cli\Queue;
use Throwable;
use yii\base\InvalidArgumentException;
use yii\di\Container;

/**
 * Класс основной логики
 *
 * Позволяет заинитить "подписщиков"
 *
 * в конструкторе регистриурем ключи и обработчики,
 * после при необходимости вызываем method notify
 * в который передаём ключ события и структуру месенджера (@see MessengerStructureContract)
 *
 * ключ представляет из  себя битовую маску, которая позволит при необходимости выполнить несколько событий
 *
 * Тесты:
 * @see \tests\unit\services\tasks\MessageSubscriberTest
 */
class MessageSubscriber implements MessageSubscriberContract, QueueableContract
{

    /**
     * @var MessengerSenderContract[]
     */
    private $observers;

    /**
     * @var Queue|null $queue
     */
    private $queue;

    /**
     * Конструктор подпищиков
     *
     * пример вызова:
     *  new MessageSubscriber(\Yii::$container,
     *   [
     *       MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM),
     *       MessengerObserverBuilder::create(ViberTunnel::class, MessengersTypeEnum::VIBER),
     *       ...
     *   ]);
     * @param Container $container
     * @param array $messengers
     */
    public function __construct(Container $container, array $messengers)
    {
        $this->queue = null;
        foreach ($messengers as $messenger) {
            if (!isset ($messenger['class'])) {
                throw new InvalidArgumentException('Messenger class undefined!');
            }
            if (!class_exists($messenger['class'])) {
                throw new InvalidArgumentException('Class:' . $messenger['class'] . ' is undefined');
            }
            if (!is_subclass_of($messenger['class'], MessengerSenderContract::class)) {
                throw new InvalidArgumentException('Class:' . $messenger['class'] .
                    ' is not implements MessengerSenderContract');
            }
            try {
                /** @var MessengerSenderContract $observer */
                $observer = $container->get($messenger['class'], $messenger['constructParams'] ?? []);
            } catch (Throwable $error) {
                throw new InvalidArgumentException('Messenger class undefined!', 500, $error);

            }
            $this->attach($observer, $messenger['messengerType']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setQueue(Queue $queue): void
    {
        $this->queue = $queue;
    }

    /**
     * {@inheritdoc}
     */
    public function getQueue(): ?Queue
    {
        return $this->queue;
    }

    /**
     * {@inheritdoc}
     */
    public function attach(MessengerSenderContract $observer, int $messengerType = 1): MessageSubscriberContract
    {
        $this->initEventGroup($messengerType);
        $this->observers[$messengerType][] = $observer;
        return $this;

    }

    /**
     * {@inheritdoc}
     */
    public function detach(MessengerSenderContract $observer, int $messengerType = 1): MessageSubscriberContract
    {
        foreach ($this->getEventObservers($messengerType) as $key => $s) {
            if ($s === $observer) {
                unset($this->observers[$messengerType][$key]);
            }
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function notify(int $messengerType = 0, MessengerStructureContract $data): array
    {
        $result = [];
        /** @var MessengerSenderContract $observer */
        foreach ($this->getEventObservers($messengerType) as $observer) {
            $result[] = $observer->update($this, $data, $this->getQueue());
        }
        return $result;
    }

    /**
     * Инициализирует группу обработчиков
     *
     * @param int $messengerType
     */
    private function initEventGroup(int $messengerType = 0)
    {
        if (!isset($this->observers[$messengerType])) {
            $this->observers[$messengerType] = [];
        }
    }

    /**
     * Возвращает группу обработчиков по типу мессенжера
     *
     * @param int $messengerType
     * @return MessengerSenderContract[]
     */
    private function getEventObservers(int $messengerType = 0): array
    {
        $this->initEventGroup($messengerType);
        $group = [];
        foreach ($this->observers as $mask => $observer) {
            if (!($messengerType & $mask) || empty($observer)) {
                continue;
            }
            $group = array_merge($group, $observer);
        }
        return $group;
    }

}