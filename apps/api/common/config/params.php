<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'maxRateLimit'=>100,
    'perRateLimit'=>300,
    'maxGetRateLimit'=>200,
    'perGetRateLimit'=>100,

];
