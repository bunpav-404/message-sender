<?php

namespace rest\versions\v1;

use common\services\tasks\MessageSubscriber;
use common\services\tasks\MessengerObserverBuilder;
use common\services\tasks\MessengersTypeEnum;
use common\services\tasks\tunnels\TelegramTunnel;
use common\services\tasks\tunnels\ViberTunnel;
use common\services\tasks\tunnels\WharsappTunnel;
use yii\base\Module;

/**
 * Class RestModule
 *
 * Модуль для АПИ версии 1
 * @package rest\versions\v1
 * @version 1
 */
class RestModule extends Module
{
    /**
     * инициализация модуля
     */
    public function init()
    {
        parent::init();
        $this->resolveDependencies();
    }

    /**
     * Установка зависимостей модуля
     */
    private function resolveDependencies(): void
    {
        \Yii::$container->set(MessageSubscriber::class,
            [],
            [
                \Yii::$container,
                [
                    MessengerObserverBuilder::create(TelegramTunnel::class, MessengersTypeEnum::TELEGRAM),
                    MessengerObserverBuilder::create(WharsappTunnel::class, MessengersTypeEnum::WHATSAPP),
                    MessengerObserverBuilder::create(ViberTunnel::class, MessengersTypeEnum::VIBER),
                ]
            ]
        );
    }

}